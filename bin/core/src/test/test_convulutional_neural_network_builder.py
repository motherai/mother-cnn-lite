import unittest
from ...src.main.service.invasiveductalcarcinoma.builder import convulutional_neural_network_builder

class TestConvulutionalNeuralNetworkBuilder(unittest.TestCase):
    
    def setUp(self) -> None:
        return None

    def test_train(self):
        print('\nTEST: test_convulutional_neural_network_builder.py -> build')

        result = convulutional_neural_network_builder.build()

        self.assertIsNotNone(result)
    

