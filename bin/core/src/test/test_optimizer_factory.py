import unittest
import torch
from ....common.src.main.utils.constants import *
from ...src.main.service.common.factory import optimizer_factory

class TestOptimizerFactory(unittest.TestCase):
    
    def setUp(self) -> None:
        return None

    def test_train(self):
        print('\nTEST: test_optimizer_factory.py -> get_optimizer')
        mock_cnn = torch.nn.Sequential(torch.nn.Linear(1, 1))

        adam = optimizer_factory.get_optimizer(mock_cnn, ADAM_OPTIMIZER)
        sgd = optimizer_factory.get_optimizer(mock_cnn, SGD_OPTIMIZER)

        self.assertIsNotNone(adam)
        self.assertIsNotNone(sgd)
    

