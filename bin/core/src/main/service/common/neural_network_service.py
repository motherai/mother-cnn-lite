import torch
from flask import current_app as app
from ......common.src.main.utils.constants import *
from ......common.src.main.utils import utils
from .factory import optimizer_factory
from ..invasiveductalcarcinoma.builder import convulutional_neural_network_builder

utils.do_seeds(SN)

'''
    Create a tensor.
    param: data
    param: dtype
    returns: tensor.  
'''
def create_tensor(data, dtype):
    return torch.tensor(data, requires_grad = False, dtype = dtype)

'''
    Initialize tensors of x and y data .
    param: x_data
    param: y_data
    returns: x_tensor, y_tensor.  
'''
def initialize_tensors(x_data, y_data):    
    x_tensor = create_tensor(x_data, torch.float)
    y_tensor = create_tensor(y_data, torch.long) 

    return x_tensor, y_tensor

'''
    Training function, will do the num setp configured in app constants.
    param: x_tensor
    param: y_tensor
    param: neural_network
    returns: Void.  
'''
def do_train(x_tensor, y_tensor, neural_network):
    initialize_weights(neural_network)
    optimizer = optimizer_factory.get_optimizer(neural_network, ADAM_OPTIMIZER)
    batch = get_batch(x_tensor, y_tensor)

    app.logger.debug("START IDC-CNN TRAINING")
    for i in range(NUM_STEPS):
        for x_tensor, y_tensor in batch:
            output = do_step(x_tensor, y_tensor, neural_network, optimizer)
        if(i%50 == 0):
            app.logger.debug(f'Epoch: {i}, Loss: {output}')
    app.logger.debug("END IDC-CC TRAINING")

'''
    Predictions of forward pass output
    param: x_tensor
    param: forward_pass
    returns: tensor.  
'''
def get_predict(x_tensor, forward_pass):
    with torch.no_grad():
        _, y_prediction = torch.max(forward_pass(x_tensor), dim=1)
    return y_prediction

'''
    Return the result of loss function.
    param: x_tensor
    param: y_tensor
    param: forward_pass
    returns: Str.  
'''
def get_loss(x_tensor, y_tesnor, forward_pass):
    return torch.nn.functional.nll_loss(forward_pass(x_tensor), y_tesnor) 

'''
    Do a step in the model training, reset derivatives values, calcule the "loss" with the results and set it to the output,
    then propagate the derivatives of our "loss" about our model params, finally the optimizer do a step.
    param: x_tensor
    param: y_tensor
    param: forward_pass
    param: optimizer
    returns: Str.  
'''
def do_step(x_tensor, y_tensor, forward_pass, optimizer):
   optimizer.zero_grad() 
   loss = get_loss(x_tensor, y_tensor, forward_pass) 
   output = loss.item()
   loss.backward()
   optimizer.step()
   return output

'''
    Return score of trained model passed by parameter in value to the tensors provided.
    param: x_tensor
    param: y_tensor
    param: forward_pass
    returns: Str.  
'''
def get_score(x_tensor, y_tensor, forward_pass):
    with torch.no_grad():
        y_prediction = get_predict(x_tensor, forward_pass)
        score = torch.sum(y_prediction == y_tensor).item() / len(y_tensor)
    return score


'''
    Function to initialize the weights with xavier function. 
    param: x_tensor
    param: y_tensor
    returns: Void.  
'''
def initialize_weights(forward_pass):
    for name, param in forward_pass.named_parameters():
        if 'weight' in name:
            torch.nn.init.xavier_normal_(param) 
        else:
            param.data.fill_(0.01)

'''
    Function to get the batch configured to be used in the training of our Sequential model.
    param: x_tensor
    param: y_tensor
    returns: Batch.  
'''
def get_batch(x_tensor, y_tensor):
    return torch.utils.data.DataLoader(torch.utils.data.TensorDataset(x_tensor, y_tensor), batch_size = BATCH_SIZE, shuffle = True)

'''
    Return the sequential of the convulutional neural network loaded by the state_dict passed by parameter.
    param: state_dict
    returns: Sequential.  
'''
def load_model(state_dict):
    model = convulutional_neural_network_builder.build()
    model.load_state_dict(state_dict)
    model = model.eval()
    return model

'''
    Activate the cuda state of tensors and sequential neural network passed by parameter.
    param: tensors
    param: neural_network
    returns: Void.  
'''
def activate_cuda(x_tensor, y_tensor, neural_network):
    x_tensor = x_tensor.cuda()
    y_tensor = y_tensor.cuda()
    neural_network = neural_network.cuda()
    return x_tensor, y_tensor, neural_network

'''
    Desactivate the cuda state of tensors and sequential neural network passed by parameter.
    param: tensors
    param: neural_network
    returns: Void.  
'''
def desactivate_cuda(x_tensor, y_tensor, neural_network):
    x_tensor = x_tensor.cpu()
    y_tensor = y_tensor.cpu()
    neural_network = neural_network.cpu()
    return x_tensor, y_tensor, neural_network