import torch
from .......common.src.main.utils.constants import *

'''
    Function to get the optimizer configured by type.
    param: type
    returns: Optimizer.
'''
def get_optimizer(neural_network, type):
    return switch_optimizer[type](neural_network)

'''
    Function to generate adam optimizer
    param: neural_network
    returns: Optimizer.
'''
def adam_optimizer(neural_network):
    return torch.optim.Adam(neural_network.parameters(), weight_decay = WEIGHT_DECAY, lr = LEARNING_RATE)

'''
    Function to generate sgd optimizer
    param: neural_network
    returns: Optimizer.
'''
def sgd_optimizer(neural_network):
    return torch.optim.SGD(neural_network.parameters(), lr = LEARNING_RATE)

switch_optimizer = {
	'adam': adam_optimizer,
    'sgd': sgd_optimizer
}





