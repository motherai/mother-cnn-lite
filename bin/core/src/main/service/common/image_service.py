import torch
import numpy as np
import PIL.Image as Image
from io import BytesIO
from skimage import transform
from ......common.src.main.utils.constants import *

'''
    Function to resize a nparray passed by parameter. (48, 48, 3) format shape will be returned, and his values 
    modified to be between -1. and 1.
    param: histological_images
    returns: npaarray. 
'''
def resize_images(histological_images): 
  histological_images_lenght = len(histological_images)
  i = 0
  histological_images_resized = [] 
  for i in range(histological_images_lenght):
    img = transform.resize(image=histological_images[i], output_shape=EXPECTED_INPUT_SHAPE)
    histological_images_resized.append(img)
    histological_images_resized[i] = histological_images_resized[i] / (np.max(histological_images_resized[i]) - np.min(histological_images_resized[i])) - VARIATION 
    i += 1
  return np.array(histological_images_resized)


'''
    Function to resize a nparray image passed by parameter. (48, 48, 3) format shape will be returned, and his values 
    modified to be between -1. and 1.
    param: histological_image
    returns: npaarray. 
'''
def resize_image(histological_image): 

  resized_histological_image = transform.resize(image=histological_image, output_shape=EXPECTED_INPUT_SHAPE)

  resized_histological_image = resized_histological_image / (np.max(resized_histological_image) - np.min(resized_histological_image)) - VARIATION 

  return np.array(resized_histological_image)

'''
    Transform nparray image to torch tensor with (BATCH_SIZE=1, RGB, H, W) output shape, 
    (H, W, RGB) shape needed. 
    returns: tensor.  
'''
def to_tensor(histological_image):
  sample = histological_image.transpose(2, 0, 1)[np.newaxis,:,:,:]
  tensor_sample = torch.tensor(sample, requires_grad = False, dtype = torch.float)
  return tensor_sample

'''
    Transform str bytes to nparray image 
    param: str_bytes
    returns: nparray.  
'''
def str_bytes_to_image(str_bytes):
  histological_image = Image.open(BytesIO(str_bytes))
  return np.array(histological_image)