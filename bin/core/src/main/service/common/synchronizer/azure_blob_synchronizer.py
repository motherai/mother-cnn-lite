import traceback
from flask import current_app as app
from .......common.src.main.utils import azure_bridge
from .......common.src.main.utils import utils
from .......common.src.main.utils.constants import *

'''
    Download the Blob name on our Azure Blob Space and save it in local resources.
    param: container_name 
    param: blob_name
    returns: Void.  
'''
def download_resource(container_name, blob_name):
    try:
        blob_client = azure_bridge.get_blob(container_name, blob_name) 

        with open(utils.get_absolute_path(f'{RESOURCE_PATH}{blob_name}'), 'wb') as resource:       
            data = blob_client.download_blob()
            resource.write(data.readall())
        app.logger.debug(f'Blob Resource: {blob_name} saved.')
    except Exception as error_message:
        app.logger.error(f'{error_message} : {traceback.format_exc()}')

'''
    Delete the Blob name on our Azure Blob Space.
    param: container_name 
    param: blob_name
    returns: Void.  
'''
def delete_resource(container_name, blob_name):
    try:
        blob_client = azure_bridge.get_blob(container_name, blob_name)
        blob_client.delete_blob()
        app.logger.debug(f'Blob Resource: {blob_name} deleted.')
    except Exception as error_message:
        app.logger.error(f'{error_message} : {traceback.format_exc()}')
      
'''
    Update the Blob name on our Azure Blob Space.
    param: container_name 
    param: blob_name
    returns: Void.  
'''
def upload_resource(container_name, blob_name, resource_name):
    try:
        blob_client = azure_bridge.get_blob(container_name, blob_name)
        blob_client.upload_blob(f'{RESOURCE_PATH}{resource_name}', overwrite = True)
        app.logger.debug(f'Blob Resource: {blob_name} updated.')
    except Exception as error_message:
        app.logger.error(f'{error_message} : {traceback.format_exc()}')


