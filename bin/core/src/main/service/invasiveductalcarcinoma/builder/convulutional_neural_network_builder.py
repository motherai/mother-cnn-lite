import torch
from .......common.src.main.utils.constants import *

'''
    Build a Squential convulutional neural network with conv2d*3 maxPool2d*2 and Linear*2. Input expected (BATCH, 3, 48, 48).
    returns: Sequential.  
'''
def build(): 
    cnn = torch.nn.Sequential(
        torch.nn.Conv2d(3, FIRST_CONV2D, kernel_size = KERNEL_SIZE, stride = STRIDE, padding = PADDING),
        torch.nn.MaxPool2d(MAX_POOL),
        torch.nn.LeakyReLU(),
    
        torch.nn.Conv2d(FIRST_CONV2D, SECOND_CONV2D, kernel_size = KERNEL_SIZE, stride = STRIDE, padding = PADDING),
        torch.nn.MaxPool2d(MAX_POOL),
        torch.nn.LeakyReLU(),

        torch.nn.Conv2d(SECOND_CONV2D, THIRD_CONV2, kernel_size = KERNEL_SIZE, stride = STRIDE, padding = PADDING),
        torch.nn.MaxPool2d(MAX_POOL),
        torch.nn.LeakyReLU(),
        torch.nn.Dropout(p = DROPOUT),

        torch.nn.Flatten(),
        torch.nn.Linear(LINEAL_FEATURES, SECOND_LINEAL_FEATURES), 
        torch.nn.LeakyReLU(),
        torch.nn.Dropout(p = DROPOUT),

        torch.nn.Linear(SECOND_LINEAL_FEATURES, OUTPUT),
        torch.nn.LogSoftmax() 
    )
    return cnn


