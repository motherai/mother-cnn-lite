import torch
from ......common.src.main.utils.constants import *
from ......common.src.main.utils import utils
from ..common import neural_network_service
from ..common import image_service
from ...persistence.invasiveductalcarcinoma import invasive_ductal_carcinoma_repository
from .builder import convulutional_neural_network_builder

'''
    Train the image data with convulutional neural network, GPU cuda needed. Input expected (x, y) Tuple.
    param: data
    returns: train_score, test_score, convulutional_neural_network trained.  
'''
def do_train(data):
    x_train, y_train, x_test, y_test = utils.split_data(data, TEST_SIZE)
    x_train, x_test = image_service.resize_images(x_train), image_service.resize_images(x_test)
    
    x_train_tensor, y_train_tensor = neural_network_service.initialize_tensors(x_train, y_train)
    x_train_tensor = x_train_tensor.transpose(INITIAL_POSITION_SHAPE_HEIGHT, INITIAL_POSITION_SHAPE_RGB)
    convulutional_neural_network = convulutional_neural_network_builder.build()

    if torch.cuda.is_available():
        x_train_tensor, y_train_tensor, convulutional_neural_network = neural_network_service.activate_cuda(x_train_tensor, y_train_tensor, convulutional_neural_network)

    neural_network_service.do_train(x_train_tensor, y_train_tensor, convulutional_neural_network)
    x_train_tensor, y_train_tensor, convulutional_neural_network = neural_network_service.desactivate_cuda(x_train_tensor, y_train_tensor, convulutional_neural_network)

    train_score = neural_network_service.get_score(x_train_tensor, y_train_tensor, convulutional_neural_network) 
    x_test_tensor, y_test_tensor = neural_network_service.initialize_tensors(x_test, y_test)

    x_test_tensor = x_test_tensor.transpose(INITIAL_POSITION_SHAPE_HEIGHT, INITIAL_POSITION_SHAPE_RGB)
    test_score = neural_network_service.get_score(x_test_tensor, y_test_tensor, convulutional_neural_network)

    return train_score, test_score, convulutional_neural_network

'''
    Return the idc prediction of histological image passed by parameter. nparray image needed. 
    param: histological_image
    returns: Str.  
'''
def get_prediction(histological_image):
    histological_image_transformed = image_service.resize_image(histological_image)
    x_tensor = image_service.to_tensor(histological_image_transformed)

    trained_model_state_dict = invasive_ductal_carcinoma_repository.get_trained_model()
    loaded_model = neural_network_service.load_model(trained_model_state_dict)

    result = neural_network_service.get_predict(x_tensor, loaded_model)
    return result



