import traceback
from flask import current_app as app
from ...service.invasiveductalcarcinoma import invasive_ductal_carcinoma_service
from ...persistence.invasiveductalcarcinoma import invasive_ductal_carcinoma_repository
from ...service.common.synchronizer import azure_blob_synchronizer
from ...service.common import image_service
from ......common.src.main.utils.constants import *

'''
    Function to get the prediction of the histological image passed by parameter
    param: histological image bytes
    returns: dict.
'''
def get(bytes):
    try:
        if not invasive_ductal_carcinoma_repository.exists_file_in_local_resources(IDC_FILE_MODEL_NAME):
            azure_blob_synchronizer.download_resource(IDC_MODEL_AZURE_CONTAINER_NAME, IDC_FILE_MODEL_NAME)

        histological_image = image_service.str_bytes_to_image(bytes)
        prediction = invasive_ductal_carcinoma_service.get_prediction(histological_image)

        result = prediction[0].item()
        app.logger.debug(f'predict result -> {result}')
    except Exception as error_message:
        result = ERROR_WHILE_PREDICTION
        app.logger.error(f'{ERROR_WHILE_PREDICTION} ->  {error_message} : {traceback.format_exc()}')
    return result
