import traceback
from flask import current_app as app
from ...service.invasiveductalcarcinoma import invasive_ductal_carcinoma_service
from ...service.common.synchronizer import azure_blob_synchronizer
from ...persistence.invasiveductalcarcinoma import invasive_ductal_carcinoma_repository
from ......common.src.main.utils.constants import *

'''
    Function to run the train of the idc_data located in the resources, if the data is not present, 
    will be downloaded from the Azure Blob Space, this function it may take +6h to finish. GPU with cuda cores needed.
    returns: dict.
'''
def idc_cnn_model():
    try:
        if not invasive_ductal_carcinoma_repository.exists_file_in_local_resources(IDC_FILE_DATA_NAME):
            azure_blob_synchronizer.download_resource(IDC_DATA_AZURE_CONTAINER_NAME, IDC_FILE_DATA_NAME)

        idc_data = invasive_ductal_carcinoma_repository.get_data()
        train_score, test_score, convulutional_neural_network = invasive_ductal_carcinoma_service.do_train(idc_data)

        invasive_ductal_carcinoma_repository.save_model(convulutional_neural_network)
        azure_blob_synchronizer.upload_resource(IDC_MODEL_AZURE_CONTAINER_NAME, IDC_FILE_MODEL_NAME, IDC_FILE_MODEL_NAME)

        result = {'train_score': train_score, 'test_score': test_score}
        app.logger.debug(f'training results -> {result}')
    except Exception as error_message:
        result = ERROR_WHILE_TRAINING
        app.logger.error(f'{ERROR_WHILE_TRAINING} ->  {error_message} : {traceback.format_exc()}')
    return result