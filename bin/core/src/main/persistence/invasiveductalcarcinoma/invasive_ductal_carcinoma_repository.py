import torch
import pickle
import os
from ......common.src.main.utils.constants import *
from ......common.src.main.utils import utils

'''
    Function to get the idc_data located in the app local repository.
    Returns: nparray.
'''
def get_data():
    with open(utils.get_absolute_path(f'{RESOURCE_PATH}{IDC_FILE_DATA_NAME}'), 'rb') as f:
        data = pickle.load(f)
    return data

'''
    Function to get the state_dict of the trained model located in local repository.
    returns: state_dict.
'''
def get_trained_model():   
    with open(utils.get_absolute_path(f'{RESOURCE_PATH}{IDC_FILE_MODEL_NAME}'), 'rb') as f:
        model = torch.load(f)
    return model

'''
    Function to save the state_dict of the trained model in local repository. 
    WARNING: This function will delete the old trained model file if exists.
    param: sequential
    returns: Void.
'''
def save_model(sequential):
    if exists_file_in_local_resources(IDC_FILE_MODEL_NAME):
        os.remove(utils.get_absolute_path(f'{RESOURCE_PATH}{IDC_FILE_MODEL_NAME}'))
    
    torch.save(sequential.state_dict(), utils.get_absolute_path(f'{RESOURCE_PATH}{IDC_FILE_MODEL_NAME}'))

'''
    Check if exists a file in local repository.
    param: file
    returns: Boolean.
'''
def exists_file_in_local_resources(file):
    path = f'{RESOURCE_PATH}{file}'
    exists = os.path.exists(path)
    return exists
