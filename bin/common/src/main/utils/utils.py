import torch
import random
import numpy as np
import os
from random import randint
from sklearn.model_selection import train_test_split

'''
    Function to initialize seeds of libraries used in the app.
    Returns:
        Void.
'''
def do_seeds(sn):
    np.random.seed(sn)
    torch.manual_seed(sn)
    random.seed(sn)

do_seeds(1)

'''
    Function to split data passed by parameter with tuple format (x, y) .
    param: data
    param: test_size
    returns: x_train, x_test, y_train, y_test
        Void.
'''
def split_data(data, test_size):
    x_all, y_all = data[0], data[1]
    x_train, x_test, y_train, y_test = train_test_split(x_all, y_all, stratify=y_all, test_size=test_size, random_state=randint(1, 200), shuffle=True)
    
    return x_train, y_train, x_test, y_test

'''
    Return the abosulte path of local_file
    param: local_file
    returns: str path.
'''
def get_absolute_path(local_file):
    return os.path.abspath(local_file)











