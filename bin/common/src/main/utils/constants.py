import json

'''app'''
RESOURCE_PATH = 'bin/core/src/main/persistence/resources/'
CONFIG_PATH = 'conf/conf.json'

with open(CONFIG_PATH, 'r') as config:
  config_json = json.load(config)

ENV = config_json['environment']

'''persistence constants'''
IDC_MODEL_AZURE_CONTAINER_NAME = 'models'
IDC_DATA_AZURE_CONTAINER_NAME = 'data'

if ENV == 'PROD':
    IDC_FILE_DATA_NAME = 'idc_data.pkl'
    IDC_FILE_MODEL_NAME = 'sequential_idc.pt'
else:
    IDC_FILE_DATA_NAME = 'idc_test_data.pkl'
    IDC_FILE_MODEL_NAME = 'test_sequential_idc.pt'

'''application constants'''
ERROR_WHILE_PREDICTION = 'ERROR-001: failed getting the prediction'
ERROR_WHILE_TRAINING = 'ERROR-002: failed training the model'

'''model constants'''
WIDTH = 48
HEIGHT = 48
KERNEL_SIZE = 3
PADDING = 2
STRIDE = 1
MAX_POOL = 2
DROPOUT = 0.25
FIRST_CONV2D = 32
SECOND_CONV2D = 64
THIRD_CONV2 = 128
LINEAL_FEATURES = 6272
SECOND_LINEAL_FEATURES = 3136
OUTPUT = 2

'''service constants'''
TEST_SIZE = float(0.2)
INITIAL_POSITION_BATCH_SIZE = 0
INITIAL_POSITION_SHAPE_HEIGHT = 1
INITIAL_POSITION_SHAPE_WEIGHT = 2
INITIAL_POSITION_SHAPE_RGB = 3

EXPECTED_INPUT_SHAPE = (HEIGHT, WIDTH, 3)
VARIATION = 0.5

'''training constants'''
WEIGHT_DECAY = 0.008
LEARNING_RATE = 0.0002
BATCH_SIZE = 64
ADAM_OPTIMIZER = 'adam'
SGD_OPTIMIZER = 'sgd'
NUM_STEPS = 500
SN = 64
