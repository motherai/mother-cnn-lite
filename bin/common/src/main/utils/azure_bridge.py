import os
from azure.storage.blob import BlobServiceClient

AZURE_CONNECTION = 'AZURE_STORAGE_CONNECTION_STRING'

'''
Retrieve the connection string for use with the application. The storage
connection string is stored in an environment variable on the machine
running the application called AZURE_STORAGE_CONNECTION_STRING. If the environment variable is
created after the application is launched in a console or with Visual Studio,
the shell or application needs to be closed and reloaded to take the
environment variable into account.
'''
connection = os.getenv(AZURE_CONNECTION)

'''
    Initialize the session with Azure Blob Service Client configured in you're environment. This client will log detailed information about its HTTP sessions, at DEBUG level
    returns: BlobServiceClient session.
'''
def get_session():
    service_client = BlobServiceClient.from_connection_string(connection)
    return service_client

'''
    Function to get the azure blob space container.
    param: container name
    returns: Container Client.
'''
def get_container(container):
    return get_session().get_container_client(container)

'''
    Function to get the azure blob space blob object.
    param: blob name
    returns: Blob Client.
'''
def get_blob(container, blob):
    return get_session().get_blob_client(container=container, blob=blob)


