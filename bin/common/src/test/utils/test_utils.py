import unittest
from ...main.utils import utils
from .....common.src.main.utils.constants import *
from .....core.src.main.persistence.invasiveductalcarcinoma import invasive_ductal_carcinoma_repository
from .....core.src.main.service.common.synchronizer import azure_blob_synchronizer

class TestUtils(unittest.TestCase):
    
    def setUp(self) -> None:
        return None

    def test_split_data(self):
        print('\nTEST: test_utils.py -> test_split_data')
        expected_len_train = 800
        expected_len_test = 200

        data = invasive_ductal_carcinoma_repository.get_data()
        x_train_result, y_train_result, x_test_result, y_test_result = utils.split_data(data, TEST_SIZE)

        self.assertEqual(len(x_train_result), expected_len_train)
        self.assertEqual(len(x_test_result), expected_len_test)
        self.assertEqual(len(y_train_result), expected_len_train)
        self.assertEqual(len(y_test_result), expected_len_test)
    

