import os
import io
import logging
from PIL import Image
from logging.handlers import RotatingFileHandler
from flask import Flask, jsonify, request
from flask_cors import CORS
from bin.core.src.main.application.invasiveductalcarcinoma import predict 
from bin.core.src.main.application.invasiveductalcarcinoma import train

# -*- coding: utf-8 -*-
app = Flask(__name__)
CORS(app)
app.config['TRAP_BAD_REQUEST_ERRORS'] = True

start_message_warning = "DEFAULT ENVIRONMENT 'DEV', AVAILABLE ENVIRONMENTS -> 'DEV', 'PROD'. "

"""
Configuration of logger file.

"""
file_handler = RotatingFileHandler('logs/mother-cnn.log')
file_handler.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.DEBUG)

app.logger.warning(start_message_warning)

# When running this app on the local machine, default the port to 5000
port = int(os.getenv('PORT', 5000))

"""
    Función para gestionar la salida de la ruta raíz.
    Returns:
        dict.  Mensaje de salida
"""
@app.route('/', methods=['GET'])
def root():

    response = {
            'Proyecto': 'SISTEMAS DE DETECCION DE CANCER DE MAMA MEDIANTE PROCESAMIENTO DE IMAGENES',
            'Curso': '2021-2022',
            'Postgrado': 'INTELIGENCIA ARTIFICIAL Y DATA SCIENCE',
            'Integrantes': 'SOFIA LLORENTE GARCIA, CLARA PALOMAR SEVILLANO, MIQUEL CASTELLS RUBIO',
            'Director': 'TENO GONZALEZ'
    }
   
    return response

"""
    Función de lanzamiento de la predicción request
    Returns:
        JSON.  result
"""
@app.route('/predict-idc', methods=['POST'])
def predict_idc():

    bytes = request.files.get('file').read()

    prediction = predict.get(bytes)

    return jsonify({'result': prediction})

"""
    Función de lanzamiento del train request
    Returns:
        JSON.  result
"""
@app.route('/train-idc-model', methods=['GET'])
def train_idc_model():

    train_result = train.idc_cnn_model()

    return jsonify({'result': train_result})

# main
if __name__ == '__main__':
    # ejecución de la app
    app.run(host='0.0.0.0', port=port, debug=True)
