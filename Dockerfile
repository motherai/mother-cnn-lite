# start by pulling the python image
FROM python:3.10.5

# copy the requirements file into the image
COPY ./templates/requirements.txt /mother-cnn/templates/requirements.txt

# switch working directory
WORKDIR /mother-cnn

# install the dependencies and packages in the requirements file
RUN pip install -r /mother-cnn/templates/requirements.txt

# copy every content from the local file to the image
COPY . ./

# add environment variable to vm
ENV FLASK_APP=run.py

# instructs to make the container available externally
CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0"]